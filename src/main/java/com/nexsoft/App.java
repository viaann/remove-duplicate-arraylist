package com.nexsoft;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ArrayList<String> goods = new ArrayList<>();
        goods.add("Indomie");
        goods.add("Popok Bayi");
        goods.add("Kapal");
        goods.add("Laptop");
        goods.add("Sabun Mandi");

        ArrayList<String> goodsNew = new ArrayList<>();
        goodsNew.add("Indomie");
        goodsNew.add("Kapal");
        goodsNew.add("Baygon");

        ArrayList<String> duplicateElement = new ArrayList<>();
        duplicateElement.addAll(goods);
        // Output = [Indomie, Kapal]
        duplicateElement.retainAll(goodsNew);

        goods.addAll(goodsNew);
        goods.removeAll(duplicateElement);

        System.out.println("==== Final Collection Goods ====");
        for (String nameGoods : goods) {
            System.out.println(nameGoods);
        }
    }
}
